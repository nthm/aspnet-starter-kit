using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarterKit.Middleware
{
    public class RequestResponseLoggingMiddleware
    {
        private readonly RequestDelegate _next;

        public RequestResponseLoggingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var request = await FormatRequest(context.Request);
            var stopWatch = Stopwatch.StartNew();
            var requestTime = DateTime.UtcNow;
            var originalBodyStream = context.Response.Body;
            using (var responseBody = new MemoryStream())
            {
                context.Response.Body = responseBody;
                await _next(context);
                stopWatch.Stop();

                var response = await FormatResponse(context.Response, stopWatch.ElapsedMilliseconds);

                Console.WriteLine($"\nRequest: {request}\nResponse: {response}");

                await responseBody.CopyToAsync(originalBodyStream);
            }
        }

        private async Task<string> FormatRequest(HttpRequest request)
        {
            var body = request.Body;
            request.EnableRewind();
            var buffer = new byte[Convert.ToInt32(request.ContentLength)];
            await request.Body.ReadAsync(buffer, 0, buffer.Length);
            var text = Encoding.UTF8.GetString(buffer);
            request.Body = body;

            string printBody = safeBody(request.ContentType, text);
            return (
                $"{request.Method} {request.Scheme}://{request.Host}{request.Path}"
                + (!String.IsNullOrWhiteSpace(request.QueryString.ToString())
                    ? $"\nQuery: {request.QueryString}"
                    : "")
                + (!String.IsNullOrWhiteSpace(printBody)
                    ? $"\nBody: {printBody}"
                    : "")
            );
        }

        private async Task<string> FormatResponse(HttpResponse response, long responseTimeMs)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            string text = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);

            string printBody = safeBody(response.ContentType, text);
            return (
                $"{response.StatusCode} {text.Length} bytes ({responseTimeMs}ms)"
                + (!String.IsNullOrWhiteSpace(printBody)
                    ? $"\nBody: {printBody}"
                    : "")
            );
        }

        private string safeBody(string mime, string content)
        {
            if (String.IsNullOrWhiteSpace(mime))
            {
                return "";
            }

            string[] substringsPrintableMIME = {
                "text", "json", "script", "html", "css", "xml"
            };
            if (!substringsPrintableMIME.Any(s => mime.Contains(s)))
            {
                return "Skipping binary data 🎞";
            }
            return content.Length < 200
                ? content
                : $"{content.Substring(0, 200)}...";
        }
    }
}
