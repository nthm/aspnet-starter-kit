On Linux .NET Core uses the root/system cert store via OpenSSL (which uses env
variables `SSL_CERT_DIR` and `SSL_CERT_FILE`). No other areas are checked. .NET
also filters out invalid or expired certificates from the store.

If you're ever installing a custom internal cert, similar to how my team does
you'll need the full chain to be installed on the system. In Docker, that means
the root and intermediate certs need to be installed.

Here are some OpenSSL commands you might find useful. Note _.crt_ is a PEM
format, in text, and _.cer_ is DER in binary.

```
# Print out all details on a certificate (maybe PEM or DER)
openssl x509 -in ./Certs/You.crt -noout -text
openssl x509 -in ./Certs/You.cer -inform der -noout -text

# Convert a .cer (DER) to .crt (PEM)
openssl x509 -in You.cer -inform der -out You.crt -outform pem

# Get the fingerprint/thumbprint
openssl x509 -noout -in You.crt -fingerprint -sha1

# Verify
openssl verify -CAfile Certs/You-RootCA.crt -untrusted Certs/You-Intermediate.crt /usr/share/ca-certificates/YourCert.pem
openssl verify -verbose -CAfile <(cat Certs/You-RootCA.crt Certs/You-Intermediate.crt) /usr/share/ca-certificates/YourCert.pem

# List all certs in the system bundle
awk -v cmd='openssl x509 -noout -subject' '
    /BEGIN/{close(cmd)};{print | cmd}' < /etc/ssl/certs/ca-certificates.crt
```
