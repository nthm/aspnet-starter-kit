# .NET Server

Partially generated with `dotnet new webapi` and then taken from EncoreFX's
Express/Maestro project.

  - NET Core 2.2
  - ASP/NET
  - Logging to Seq via Serilog
  - NUnit testing
  - Swagger API doc generation
  - MVC design with a starter controller
  - EntityFramework database ORM
  - Statically serves the UI
  - JSON configuration using _appsettings.json_ and environment variables
